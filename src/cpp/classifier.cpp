 //////////////////////////////////////////////////////////////////////////////
//                                    Libraries                             //
//////////////////////////////////////////////////////////////////////////////
#include <hls_stream.h>
#include "config.h"


//////////////////////////////////////////////////////////////////////////////
//                                     Function                             //
//////////////////////////////////////////////////////////////////////////////
// This function takes as input both time and frequency features and
// classify the observation, distinguishing between stressed and not
// stressed condition.
void classifier(hls::stream<time_feat_data> &stream_out_time,
				hls::stream<freq_feat_data> &stream_out_freq,
				hls::stream<class_data> &out_class){

	int i; // indexes
	const float A = 0.9971;
	const float B = 0.0668;
	const float C = 0.0013;
	const float D = 0.0258;
	const float E = 0.0047;

	class_data output;
	time_feat_data val_time;
	freq_feat_data val_freq;
	float cum;

	for(i=0; i<T-1; i++){
#pragma HLS pipeline
		val_time = stream_out_time.read();
		val_freq = stream_out_freq.read();

		cum = val_time.mRR*A - val_time.mHR*B + val_freq.SVI*C +
				val_freq.nLF*D - val_freq.nHF *D - val_freq.dLFHF*E;

		if(cum>THRESH){
			output.data = 0.0; // 0 == not stressed;
		}
		else{
			output.data = 1.0; // 1 == stressed;
		}
		output.last = 0;
		out_class.write(output);
	}

	val_time = stream_out_time.read();
	val_freq = stream_out_freq.read();

	cum = val_time.mRR*A - val_time.mHR*B + val_freq.SVI*C +
					val_freq.nLF*D - val_freq.nHF *D - val_freq.dLFHF*E;

	if(cum>THRESH){
		output.data = 0.0; // 0 == not stressed;
	}
	else{
		output.data = 1.0; // 1 == stressed;
	}
	output.last = 1;
	out_class.write(output);
}
