//////////////////////////////////////////////////////////////////////////////
//                                    Libraries                             //
//////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hls_stream.h>
#include "config.h"


//////////////////////////////////////////////////////////////////////////////
//                                   Functions                              //
//////////////////////////////////////////////////////////////////////////////
void flow_interface(hls::stream<float> &stream_in,
		hls::stream<class_data> &out_class);

//////////////////////////////////////////////////////////////////////////////
//                                      Main                                //
//////////////////////////////////////////////////////////////////////////////
int main(){
	hls::stream<float> stream_in("stream_in");
	hls::stream<class_data> out_class("classifier");
	class_data output_val;
	FILE* fp;
	FILE* fp1;
	int i = 0;
	float rr;
	float pxx;

	//Read the rr in input
	printf("Reading rr...\n");
	fp = fopen("rr_tohw.txt", "r");
	if(fp == NULL){
		printf("Error reading file\n");
		return 1;
	}
	while(!feof(fp) && i<N){
		fscanf(fp, "%f\n", &(rr));
		stream_in.write(rr);
		i++;
	}
	fclose(fp);

	i = 0;
	//Read the spectra in input
	printf("Reading spectra...\n");
	fp1= fopen("spectra.txt", "r");
	if(fp1 == NULL){
		printf("Error reading file\n");
		return 1;
	}
	while(!feof(fp1) && i<FFT){
		fscanf(fp1, "%f\n", &(pxx));
		stream_in.write(pxx);
		i++;
	}
	fclose(fp1);

	flow_interface(stream_in, out_class);

	//Print classifier output
	i = 0;
	printf("Classifier output:\n");
	while(!i){
		output_val = out_class.read();
		printf("%f\n", output_val.data);
		i = output_val.last;
	}

	return 0;
}
