//////////////////////////////////////////////////////////////////////////////
//                                    Libraries                             //
//////////////////////////////////////////////////////////////////////////////
#include <hls_stream.h>
#include "config.h"


//////////////////////////////////////////////////////////////////////////////
//                                     Function                             //
//////////////////////////////////////////////////////////////////////////////
void stream_in_split(hls::stream<float> &stream_in,
		hls::stream<float> &rr, hls::stream<float> &spectra){

		int i;
		float tmp;

		for(i=0; i<N; i++){
#pragma HSL pipeline
			tmp = stream_in.read();
			rr.write(tmp);
		}

		for(i=0; i<FFT; i++){
#pragma HLS pipeline
			tmp = stream_in.read();
			spectra.write(tmp);
		}
}
