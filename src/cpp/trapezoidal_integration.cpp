//////////////////////////////////////////////////////////////////////////////
//                                    Libraries                             //
//////////////////////////////////////////////////////////////////////////////
#include <hls_stream.h>
#include "config.h"


//////////////////////////////////////////////////////////////////////////////
//                                     Define                               //
//////////////////////////////////////////////////////////////////////////////
#define W 8
#define height 0.033333333
#define MIN 3
#define MAX 5


//////////////////////////////////////////////////////////////////////////////
//                                     Function                             //
//////////////////////////////////////////////////////////////////////////////
// This function takes as input the spectra and exploit traezoidal integration
// technique to extract low and high frequencies area
// Area under FFT spectra at those frequencies is calculated approximating it
// with the series of many trapezes
void trapezoidal_integration(hls::stream<float> &spectrum_sample, hls::stream<lh_hf_data> &area){
//#pragma HLS INTERFACE axis port = spectrum_sample
//#pragma HLS INTERFACE axis port = area
//#pragma HLS INTERFACE ap_ctrl_none port = return

	float tot_spectra[W]; // spectra samples (normalized, from 0 to 0.5Hz)
#pragma HLS array_partition variable=tot_spectra block factor=8

	float low_area; // area at low frequencies
	float high_area; // area at high frequencies
	float a, b; // contribute to area calculation
	lh_hf_data output; // output data structure. It contains LF area, HF area and last flag
	int i,j; // indexes

	for(j=0; j<T; j++){
		low_area = 0; // reset LF area value
		high_area = 0; // reset HF area value

		for(i=0; i<W; i++){
#pragma HLS pipeline
			tot_spectra[i] = spectrum_sample.read(); // read the spectra samples
		}

		low_area = ((tot_spectra[1]+tot_spectra[2])*height)/2; // calculate the new trapezoidal area
		output.lf = low_area; // write LF area in the output data

		a = ((tot_spectra[3]+tot_spectra[4])*height)/2; // calculate the new trapezoidal area
		b = ((tot_spectra[4]+tot_spectra[5])*height)/2; // calculate the new trapezoidal area
		high_area = a + b; // add it to the HF area estimation

		output.hf = high_area; // write LF area in the output data
		area.write(output); // write the filled data in the output stream
	}
}
