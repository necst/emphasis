//////////////////////////////////////////////////////////////////////////////
//                                     Define                               //
//////////////////////////////////////////////////////////////////////////////
#define N 1024
#define FFT (N/2)
#define T (N/16)
#define THRESH 599

// This structure contains the time-domain features for the classification task
struct time_feat_data{
  float mRR;
  float mHR;
};

// This structure contains the frequency-domain features for the classification task
struct freq_feat_data{
  float SVI;
  float dLFHF;
  float nLF;
  float nHF;
};

// This structure contains the area of low frequencies and high frequencies in a spectra.
struct lh_hf_data{
  float lf;
  float hf;
};

// This structure contains the classification output values
struct class_data{
  float data;
  bool last; // if 1 indicates that it is the last output value
};
