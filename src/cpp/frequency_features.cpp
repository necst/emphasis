//////////////////////////////////////////////////////////////////////////////
//                                    Libraries                             //
//////////////////////////////////////////////////////////////////////////////
#include <hls_stream.h>
#include "config.h"


//////////////////////////////////////////////////////////////////////////////
//                                     Function                             //
//////////////////////////////////////////////////////////////////////////////
// This function computes:
// SVI = LF/HF; nHF = HF/(LF+HF); nLF = LF/(LF+HF); dLFHF = |HF-LF|
// taking ad input LF and HF that are the area of low frequencies and the area
// of high frequencies of the spectra respectively.
void frequency_features(hls::stream<lh_hf_data> &area, hls::stream<freq_feat_data> &freq_feat){
//#pragma HLS INTERFACE axis port = area
//#pragma HLS INTERFACE axis port = freq_feat
//#pragma HLS INTERFACE ap_ctrl_none port = return

	int i = 0; // Set to 1 when the last input data is read from the stream
	float lf_value; //area of low frequencies
	float hf_value; //area of high frequencies
	float tot; //lf+hf
	float nlf; // area of low frequencies normalized by tot
	float nhf; // area of high frequencies normalized by tot
	freq_feat_data output; // output data
	lh_hf_data input; // input data

	for(i=0; i<T; i++){
#pragma HLS pipeline
		input = area.read(); // read the data from the stream
		lf_value = input.lf; // extract low frequencies content
		hf_value = input.hf; // extract high frequencies content
		tot = lf_value + hf_value;
		output.SVI = lf_value/hf_value;
		nlf = (lf_value/tot)*100;
		nhf = (hf_value/tot)*100;
		output.nLF = nlf;
		output.nHF = nhf;
		output.dLFHF = nhf-nlf;
		freq_feat.write(output);
	}
}
