//////////////////////////////////////////////////////////////////////////////
//                                    Libraries                             //
//////////////////////////////////////////////////////////////////////////////
#include <hls_stream.h>
#include "config.h"


//////////////////////////////////////////////////////////////////////////////
//                                   Functions                              //
//////////////////////////////////////////////////////////////////////////////
void stream_in_split(hls::stream<float> &stream_in,
		hls::stream<float> &rr,
		hls::stream<float> &spectra);
void time_features(hls::stream<float> &rr,
		hls::stream<time_feat_data> &time_feat);
void trapezoidal_integration(hls::stream<float> &spectra,
		hls::stream<lh_hf_data> &area);
void frequency_features(hls::stream<lh_hf_data> &area,
		hls::stream<freq_feat_data> &freq_feat);
void classifier(hls::stream<time_feat_data> &stream_out_time,
				hls::stream<freq_feat_data> &stream_out_freq,
				hls::stream<class_data> &out_class);

//////////////////////////////////////////////////////////////////////////////
//                                     Function                             //
//////////////////////////////////////////////////////////////////////////////
// This function computes:
// SVI = LF/HF; nHF = HF/(LF+HF); nLF = LF/(LF+HF); dLFHF = |HF-LF|
// taking ad input LF and HF that are the area of low frequencies and the area
// of high frequencies of the spectra respectively.
void flow_interface(hls::stream<float> &stream_in,
		hls::stream<class_data> &out_class){
#pragma HLS INTERFACE axis port = stream_in
#pragma HLS INTERFACE axis port = out_class
#pragma HLS INTERFACE ap_ctrl_none port = return

		hls::stream<float> rr("rr");
		hls::stream<float> spectra("spectra");
		hls::stream<lh_hf_data> area("area");
		hls::stream<time_feat_data> stream_out_time("time_features");
		hls::stream<freq_feat_data> stream_out_freq("freq_features");

#pragma DATAFLOW
#pragma HLS stream depth=1536 variable=stream_in
#pragma HLS stream depth=1024 variable=rr
#pragma HLS stream depth=512 variable=spectra
#pragma HLS stream depth=64 variable=area
#pragma HLS stream depth=64 variable=stream_out_time
#pragma HLS stream depth=64 variable=stream_out_freq
#pragma HLS stream depth=64 variable=out_class

		// Input stream split
		stream_in_split(stream_in, rr, spectra);

		// Time features calculation
		time_features(rr, stream_out_time);

		// Frequency features calculation
		trapezoidal_integration(spectra, area);
		frequency_features(area, stream_out_freq);

		//Classifier output calculation
		classifier(stream_out_time, stream_out_freq, out_class);

}
