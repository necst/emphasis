//////////////////////////////////////////////////////////////////////////////
//                                    Libraries                             //
//////////////////////////////////////////////////////////////////////////////
#include <hls_stream.h>
#include "config.h"

//////////////////////////////////////////////////////////////////////////////
//                                     Define                               //
//////////////////////////////////////////////////////////////////////////////
#define factor 60000
#define W 16


//////////////////////////////////////////////////////////////////////////////
//                                     Function                             //
//////////////////////////////////////////////////////////////////////////////
// This function compute the time domain features for the classification taks
void time_features(hls::stream<float> &rr,
		hls::stream<time_feat_data> &time_feat){
//#pragma HLS INTERFACE axis port = rr
//#pragma HLS INTERFACE axis port = time_feat
//#pragma HLS INTERFACE ap_ctrl_none port = return

	time_feat_data output; // output data containing the mean RR, the mean HR value and the last flag
	int i; // index
	int j; // index
	float tmp;
	float mean_rr = 0; // stores the mean of each window of 32 RR intervals
	float mean_hr = 0; // stores the mean of each window of 32 HR intervals
	for(j=0; j<T; j++){
		mean_rr = 0;
		mean_hr = 0;

		for(i=0; i<W; i++){
			tmp = rr.read();
			mean_rr = mean_rr + tmp; // accumulate it in the mean
			mean_hr = mean_hr + 1/tmp; // accumulate the HR in the mean
		}

		output.mRR = mean_rr/W; // divide to effectively compute mean RR value
		output.mHR = mean_hr/W; // divide to effectively compute mean HR value
		time_feat.write(output); // write the output data constructed on the output stream
	}

}
